#include <obs-module.h>
#include <graphics/image-file.h>
#include <graphics/matrix4.h>
#include <util/platform.h>
#include <util/dstr.h>
#include <sys/stat.h>
#include <windows.h>
#include <obs.h>
#include "../../UI/obs-frontend-api/obs-frontend-api.h"
#include <list>

#include <curl/curl.h>
#include <fstream>
#include <sstream>
#include <iostream>


/*#define blog(log_level, format, ...) \
	blog(log_level, "[image_source: '%s'] " format, \
			obs_source_get_name(context->source), ##__VA_ARGS__)*/

#define debug(format, ...) \
	blog(LOG_DEBUG, format, ##__VA_ARGS__)
#define info(format, ...) \
	blog(LOG_INFO, format, ##__VA_ARGS__)
#define warn(format, ...) \
	blog(LOG_WARNING, format, ##__VA_ARGS__)



struct image_source {
	obs_source_t *source;

	char         *file;
	bool         persistent;
	time_t       file_timestamp;
	float        update_time_elapsed;
	uint64_t     last_time;
	bool         active;

	obs_source_t* sceneSource;
	obs_scene_t* scene;
	obs_sceneitem_t* sceneItem;
	bool		downloaded;

	gs_image_file_t image;
};

struct enumSceneItem {
	obs_sceneitem_t* item;
	vec2 size;
	vec2 screenSize;
	vec2 pos;
	vec2 scale;
	int currentIndex;
	int sourceItemIndex;
	bool result;
};


static size_t data_write(void* buf, size_t size, size_t nmemb, void* userp)
{
	if (userp)
	{
		std::ostream& os = *static_cast<std::ostream*>(userp);
		std::streamsize len = size * nmemb;
		if (os.write(static_cast<char*>(buf), len))
			return len;
	}

	return 0;
}


CURLcode curl_read(const std::string& url, std::ostream& os, long timeout = 30)
{
	CURLcode code(CURLE_FAILED_INIT);
	CURL* curl = curl_easy_init();

	if (curl)
	{
		if (CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &data_write))
			&& CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L))
			&& CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L))
			&& CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_FILE, &os))
			&& CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout))
			&& CURLE_OK == (code = curl_easy_setopt(curl, CURLOPT_URL, url.c_str())))
		{
			code = curl_easy_perform(curl);
			if (code != CURLE_OK) {
				blog(LOG_INFO, "curl_easy_perform() failed: %s\n", curl_easy_strerror(code));
			}
		}

		curl_easy_cleanup(curl);
	}
	return code;
}

// Image URL
std::string url = "http://5.imimg.com/data5/TB/IF/MY-41399105/potato-500x500.jpg";



static void release_sceneSource(image_source* context) {
	if (context->sceneSource != NULL) {
		obs_source_release(context->sceneSource);
		context->sceneSource = NULL;
	}
}

static time_t get_modified_timestamp(const char *filename)
{
	struct stat stats;
	if (os_stat(filename, &stats) != 0)
		return -1;
	return stats.st_mtime;
}

static const char *image_source_get_name(void *unused)
{
	UNUSED_PARAMETER(unused);
	return obs_module_text("CoveryPlugin");
}

static void image_source_load(struct image_source *context)
{
	char *file = context->file;

	obs_enter_graphics();
	gs_image_file_free(&context->image);
	obs_leave_graphics();

	if (file && *file) {
		debug("loading texture '%s'", file);
		context->file_timestamp = get_modified_timestamp(file);
		gs_image_file_init(&context->image, file);
		context->update_time_elapsed = 0;

		obs_enter_graphics();
		gs_image_file_init_texture(&context->image);
		obs_leave_graphics();

		if (!context->image.loaded)
			warn("failed to load texture '%s'", file);
	}
}

static void image_source_unload(struct image_source *context)
{
	obs_enter_graphics();
	gs_image_file_free(&context->image);
	obs_leave_graphics();
}



static void image_source_update(void *data, obs_data_t *settings)
{
	struct image_source *context = (image_source*)data;
	const char *file = obs_data_get_string(settings, "file");
	//const char *file = "e:/output.jpg";
	const bool unload = obs_data_get_bool(settings, "unload");
	//context->downloaded = false;


	/*curl_global_init(CURL_GLOBAL_ALL);

	std::ofstream ofs("e:/output.jpg", std::ostream::binary);

	if (CURLE_OK == curl_read(url, ofs))
	{
		// Image successfully written to file
	}

	curl_global_cleanup();*/


	if (context->file)
		bfree(context->file);
	context->file = bstrdup(file);
	context->persistent = !unload;

	/* Load the image if the source is persistent or showing */
	if (context->persistent || obs_source_showing(context->source))
		image_source_load((image_source*)data);
	else
		image_source_unload((image_source*)data);
}

static void image_source_defaults(obs_data_t *settings)
{
	obs_data_set_default_bool(settings, "unload", false);
}

static void image_source_show(void *data)
{
	struct image_source *context = (image_source*)data;

	if (!context->persistent) {
		image_source_load(context);
	}
}

static void image_source_hide(void *data)
{
	struct image_source *context = (image_source*)data;

	if (!context->persistent)
		image_source_unload(context);
}

static void *image_source_create(obs_data_t *settings, obs_source_t *source)
{
	struct image_source *context = (image_source*)bzalloc(sizeof(struct image_source));
	context->source = source;

	image_source_update(context, settings);

	//MessageBox(NULL, (LPCWSTR)L"Hello", (LPCWSTR)L"Welcome Message", MB_OK);
	
	//obs_scene_enum_items(obs_scene_t *scene, bool(*callback)(obs_scene_t*, obs_sceneitem_t*, void*), void *param)


	return context;
}


static void image_source_destroy(void *data)
{
	struct image_source *context = (image_source*)data;


	//obs_scene_release(scene);
	//obs_source_release(sceneSource);
	//obs_sceneitem_release(sceneItem);


	image_source_unload(context);

	if (context->file)
		bfree(context->file);
	bfree(context);
}

static uint32_t image_source_getwidth(void *data)
{
	struct image_source *context = (image_source*)data;
	//blog(LOG_INFO, "WIDTH: %d", context->image.cx);
	return context->image.cx;
}

static uint32_t image_source_getheight(void *data)
{
	struct image_source *context = (image_source*)data;
	//blog(LOG_INFO, "HEIGHT: %d", context->image.cy);
	return context->image.cy;
}

static void image_source_render(void *data, gs_effect_t *effect)
{
	struct image_source *context = (image_source*)data;

	if (!context->image.texture)
		return;

	gs_effect_set_texture(gs_effect_get_param_by_name(effect, "image"),
			context->image.texture);
	gs_draw_sprite(context->image.texture, 0,
			context->image.cx, context->image.cy);
}

static void image_source_tick(void *data, float seconds)
{
	struct image_source *context = (image_source*)data;
	uint64_t frame_time = obs_get_video_frame_time();

	struct matrix4 dst;
	gs_matrix_get(&dst);

	//blog(LOG_INFO, "X: %d", dst.x);
	

	context->update_time_elapsed += seconds;

	if (context->update_time_elapsed >= 1.0f) {
		time_t t = get_modified_timestamp(context->file);
		context->update_time_elapsed = 0.0f;

		if (context->file_timestamp != t) {
			image_source_load(context);
		}
	}

	if (obs_source_active(context->source)) {
		if (!context->active) {
			if (context->image.is_animated_gif)
				context->last_time = frame_time;
			context->active = true;
		}

		


		/*context->sceneSource = obs_frontend_get_current_scene();
		context->scene = obs_scene_from_source(context->sceneSource);

		context->sceneItem = obs_scene_find_source(context->scene, obs_source_get_name(context->source));
		if (context->sceneItem == nullptr) {
			obs_source_release(context->sceneSource);
			return;
		}

		//obs_sceneitem_release(context->sceneItem);
		
		bool isVisible = obs_sceneitem_visible(context->sceneItem);

		//blog(LOG_INFO, "igaz: %d", isVisible);



		enumSceneItem *enumItem = (enumSceneItem*)malloc(sizeof(enumSceneItem));
		enumItem->screenSize.x = obs_source_get_width(context->sceneSource);
		enumItem->screenSize.y = obs_source_get_height(context->sceneSource);
		enumItem->size.x = obs_source_get_width(context->source);
		enumItem->size.y = obs_source_get_height(context->source);
		//enumItem->item = sceneItem;
		obs_sceneitem_get_scale(context->sceneItem, &enumItem->scale);
		obs_sceneitem_get_pos(context->sceneItem, &enumItem->pos);

		//obs_scene_enum_items(scene, bool(*callback)(obs_scene_t*, obs_sceneitem_t*, void*), void *param)
	
		//obs_sceneitem_set_order_position(sceneItem, 1000);

		obs_sceneitem_set_order(context->sceneItem, OBS_ORDER_MOVE_TOP);


		blog(LOG_INFO, "X_POS: %f | Y_POS: %f | X: %f | Y: %f | SX: %f | SY: %f", enumItem->pos.x, enumItem->pos.y, enumItem->size.x, enumItem->size.y, enumItem->scale.x, enumItem->scale.y);

		if (enumItem->pos.x+(enumItem->size.x*enumItem->scale.x) > enumItem->screenSize.x) {
			vec2 newVec2;
			newVec2.x = enumItem->screenSize.x-(enumItem->size.x*enumItem->scale.x);
			newVec2.y = enumItem->pos.y;
			obs_sceneitem_set_pos(context->sceneItem, &newVec2);
		}

		if (enumItem->pos.y+(enumItem->size.y*enumItem->scale.y) > enumItem->screenSize.y) {
			vec2 newVec2;
			newVec2.x = enumItem->pos.x;
			newVec2.y = enumItem->screenSize.y-(enumItem->size.y*enumItem->scale.y);
			obs_sceneitem_set_pos(context->sceneItem, &newVec2);
		}

		// CENTER 
		{
			vec2 newVec2;
			newVec2.x = enumItem->screenSize.x/2-(enumItem->size.x*enumItem->scale.x)/2;
			newVec2.y = enumItem->screenSize.y/2-(enumItem->size.y*enumItem->scale.y)/2;
			obs_sceneitem_set_pos(context->sceneItem, &newVec2);
		}

		free(enumItem);
		obs_source_release(context->sceneSource); */

	} else {
		if (context->active) {
			if (context->image.is_animated_gif) {
				context->image.cur_frame = 0;
				context->image.cur_loop = 0;
				context->image.cur_time = 0;

				obs_enter_graphics();
				gs_image_file_update_texture(&context->image);
				obs_leave_graphics();
			}

			context->active = false;
		}

		return;
	}

	if (context->last_time && context->image.is_animated_gif) {
		uint64_t elapsed = frame_time - context->last_time;
		bool updated = gs_image_file_tick(&context->image, elapsed);

		if (updated) {
			obs_enter_graphics();
			gs_image_file_update_texture(&context->image);
			obs_leave_graphics();
		}
	}

	context->last_time = frame_time;
}


static const char *image_filter =
	"All formats (*.bmp *.tga *.png *.jpeg *.jpg *.gif *.psd);;"
	"BMP Files (*.bmp);;"
	"Targa Files (*.tga);;"
	"PNG Files (*.png);;"
	"JPEG Files (*.jpeg *.jpg);;"
	"GIF Files (*.gif);;"
	"PSD Files (*.psd);;"
	"All Files (*.*)";

static obs_properties_t *image_source_properties(void *data)
{
	struct image_source *s = (image_source*)data;
	struct dstr path = {0};

	obs_properties_t *props = obs_properties_create();

	if (s && s->file && *s->file) {
		const char *slash;

		dstr_copy(&path, s->file);
		dstr_replace(&path, "\\", "/");
		slash = strrchr(path.array, '/');
		if (slash)
			dstr_resize(&path, slash - path.array + 1);
	}

	obs_properties_add_path(props,
			"file", obs_module_text("File"),
			OBS_PATH_FILE, image_filter, path.array);
	obs_properties_add_bool(props,
			"unload", obs_module_text("UnloadWhenNotShowing"));
	obs_properties_add_text(props,"streamer_key", obs_module_text("StreamerKey"), OBS_TEXT_DEFAULT);
	dstr_free(&path);

	return props;
}


/*static struct obs_source_info covery_plugin_info = {
	.id             = "covery_plugin",
	.type           = OBS_SOURCE_TYPE_INPUT,
	.output_flags   = OBS_SOURCE_VIDEO,
	.get_name       = image_source_get_name,
	.create         = image_source_create,
	.destroy        = image_source_destroy,
	.update         = image_source_update,
	.get_defaults   = image_source_defaults,
	.show           = image_source_show,
	.hide           = image_source_hide,
	.get_width      = image_source_getwidth,
	.get_height     = image_source_getheight,
	.video_render   = image_source_render,
	.video_tick     = image_source_tick,
	.get_properties = image_source_properties,
};*/

OBS_DECLARE_MODULE()
OBS_MODULE_USE_DEFAULT_LOCALE("covery-plugin", "en-US")
MODULE_EXPORT const char *obs_module_description(void)
{
	return "Covery plugin";
}

extern struct obs_source_info slideshow_info;

bool obs_module_load(void)
{

	struct obs_source_info covery_plugin_info = {};
	covery_plugin_info.id = "covery_plugin";
	covery_plugin_info.type = OBS_SOURCE_TYPE_INPUT;
	covery_plugin_info.output_flags = OBS_SOURCE_VIDEO;
	covery_plugin_info.get_name = image_source_get_name;
	covery_plugin_info.create = image_source_create;
	covery_plugin_info.destroy = image_source_destroy;
	covery_plugin_info.update = image_source_update;
	covery_plugin_info.get_defaults = image_source_defaults;
	covery_plugin_info.show = image_source_show;
	covery_plugin_info.hide = image_source_hide;
	covery_plugin_info.get_width = image_source_getwidth;
	covery_plugin_info.get_height = image_source_getheight;
	covery_plugin_info.video_render = image_source_render;
	covery_plugin_info.video_tick = image_source_tick;
	covery_plugin_info.get_properties = image_source_properties;


	obs_register_source(&covery_plugin_info);
	return true;
}



